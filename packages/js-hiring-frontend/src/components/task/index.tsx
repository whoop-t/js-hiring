import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { ITask } from '../../interfaces/task.interface';

const task = ({ task, index }: { task: ITask; index: number }) => {
  return (
    <Draggable key={task._id} draggableId={task._id} index={index}>
      {(provided, snapshot) => {
        return (
          <div
            ref={provided.innerRef}
            {...provided.draggableProps}
            {...provided.dragHandleProps}
            style={{
              userSelect: 'none',
              padding: '2px 16px 2px 16px',
              margin: '0 0 8px 0',
              minHeight: '50px',
              backgroundColor: snapshot.isDragging ? '#263B4A' : '#456C86',
              color: 'white',
              ...provided.draggableProps.style
            }}
          >
            <p>title: {task.title}</p>
            <p>desc: {task.description}</p>
          </div>
        );
      }}
    </Draggable>
  );
};

export default task;
