import React, { useState } from 'react';
import { Droppable } from 'react-beautiful-dnd';
import { ITask } from '../../interfaces/task.interface';
import config from '../../util/config/config';
import Modal from 'react-modal';
import Task from '../task';
import './column.css';

// Styles for modal
const customStyles = {
  content: {
    top: '50%',
    left: '50%',
    right: 'auto',
    bottom: 'auto',
    marginRight: '-50%',
    transform: 'translate(-50%, -50%)'
  }
};

const Column = ({ columnId, column, addTask }: { columnId: string; column: any; addTask: (id: string, newTask: ITask) => void }) => {
  const [showModal, setShowModal] = useState(false);
  const [title, setTitle] = useState('');
  const [desc, setDesc] = useState('');
  const [error, setError] = useState(false);

  /**
   * onAddTask
   * @desc POST newly created task, call function in parent to update local state with new task
   */
  const onAddTask = async () => {
    if (!title || !desc) {
      setError(true);
      return;
    }
    try {
      const response = await fetch(config.url, {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify({
          title: title,
          description: desc,
          status: column.name,
          board_id: columnId
        })
      });
      const { task } = await response.json();
      addTask(columnId, task);
      setShowModal(false);
    } catch (error) {}
  };

  const onOpenModal = () => {
    setShowModal(true);
  };

  const closeModal = () => {
    setError(false);
    setShowModal(false);
  };

  return (
    <div className='column' key={columnId}>
      <h2>{column.name}</h2>
      <div style={{ margin: 8 }}>
        <Droppable droppableId={columnId} key={columnId}>
          {(provided, snapshot) => {
            return (
              <div
                {...provided.droppableProps}
                ref={provided.innerRef}
                className='inner-column'
                style={{
                  background: snapshot.isDraggingOver ? 'lightblue' : 'lightgrey'
                }}
              >
                {column.tasks.map((task: ITask, index: number) => {
                  return <Task task={task} index={index}></Task>;
                })}
                {provided.placeholder}
              </div>
            );
          }}
        </Droppable>
      </div>
      <button onClick={onOpenModal}>+ add task</button>
      {showModal && (
        <Modal isOpen={showModal} style={customStyles}>
          <div className='modal-content'>
            <label htmlFor='title'>title</label>
            <input type='text' onChange={e => setTitle(e.target.value)} />
            <label htmlFor='desc'>desc</label>
            <input type='text' onChange={e => setDesc(e.target.value)} />
            <div>
              <button onClick={closeModal}>Cancel</button>
              <button onClick={onAddTask}>Submit</button>
            </div>
            {error && <p>Please fill fields</p>}
          </div>
        </Modal>
      )}
    </div>
  );
};

export default Column;
