interface IConfig {
  url: string;
}

const config: IConfig = {
  url: 'http://localhost:4000/'
};

export default config;
