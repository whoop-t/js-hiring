export interface ITask {
  _id: string;
  title: string;
  description: string;
  status: string;
  board_id: string;
}

// export default class Task {
//   title: string;
//   description: string;
//   status: string;
//   boardId: string;
//   constructor(properties: ITask) {
//     this.title = properties.title;
//     this.description = properties.description;
//     this.status = properties.status;
//     this.boardId = properties.board_id;
//   }
// }
