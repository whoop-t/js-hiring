import React, { useEffect, useState } from 'react';
import Column from './components/column';
import { DragDropContext, DragUpdate } from 'react-beautiful-dnd';
import { ITask } from './interfaces/task.interface';
import config from './util/config/config';
import './App.css';

// Hardcoded columns, would have in DB if more robust
const hardCodedColumns: any = {
  '0': {
    name: 'Ready',
    tasks: []
  },
  '1': {
    name: 'In Progress',
    tasks: []
  },
  '2': {
    name: 'QA',
    tasks: []
  },
  '3': {
    name: 'Complete',
    tasks: []
  }
};

/**
 * onDragEnd
 * @param result
 * @param columns
 * @param setColumns
 * @desc Move draggable component to new column
 */
const onDragEnd = async (result: DragUpdate, columns: any, setColumns: any) => {
  if (!result.destination) return;
  const { source, destination, draggableId } = result;

  if (source.droppableId !== destination.droppableId) {
    updateTaskColumns(destination.droppableId, draggableId, columns[destination.droppableId].name);
    const sourceColumn = columns[source.droppableId];
    const destColumn = columns[destination.droppableId];
    const sourceTasks = [...sourceColumn.tasks];
    const destTasks = [...destColumn.tasks];
    const [removed] = sourceTasks.splice(source.index, 1);
    destTasks.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...sourceColumn,
        tasks: sourceTasks
      },
      [destination.droppableId]: {
        ...destColumn,
        tasks: destTasks
      }
    });
  } else {
    const column = columns[source.droppableId];
    const copiedTasks = [...column.tasks];
    const [removed] = copiedTasks.splice(source.index, 1);
    copiedTasks.splice(destination.index, 0, removed);
    setColumns({
      ...columns,
      [source.droppableId]: {
        ...column,
        tasks: copiedTasks
      }
    });
  }
};

/**
 * updateTaskColumn
 * @param boardId
 * @param id
 * @desc patch the doc in DB with updated boardId and status for persistence
 */
const updateTaskColumns = async (boardId: string, id: string, status: string) => {
  console.log(status);

  try {
    const response = await fetch(config.url, {
      method: 'PATCH',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        boardId,
        status,
        id
      })
    });
    // Task with old location, if needed
    const task = await response.json();
  } catch (err) {
    console.log(err);
  }
};

const App = () => {
  const [columns, setColumns] = useState(hardCodedColumns);
  const [loading, setLoading] = useState<boolean>(false);

  /**
   * constructPersistentColumns
   * @param tasks
   * @desc Put task returned from api in proper columns
   */
  const constructPersistentColumns = (tasks: ITask[]) => {
    let newColumns = { ...columns };
    for (const task of tasks) {
      // Spread task in column with matching id
      newColumns[task.board_id].tasks = [...newColumns[task.board_id].tasks, task];
    }
    setColumns(newColumns);
  };

  /**
   * fetchTasks
   * @desc Get Tasks from DB
   */
  const fetchTasks = async () => {
    setLoading(true);
    try {
      const response = await fetch(config.url);
      const { tasks }: { tasks: ITask[] } = await response.json();
      if (tasks.length) constructPersistentColumns(tasks);
    } catch (error) {
      console.log(error);
    }
    setLoading(false);
  };

  /**
   * addTask
   * @param id
   * @param newTask
   * @desc Add newly created task to local state/columns
   */
  const addTask = (id: string, newTask: ITask) => {
    const column = columns[id];
    const copiedTasks = [...column.tasks];

    setColumns({
      ...columns,
      [id]: {
        ...column,
        tasks: [...copiedTasks, newTask]
      }
    });
  };

  useEffect(() => {
    fetchTasks();
  }, []);

  if (loading) {
    return <div>loading...</div>;
  }
  if (!loading) {
    return (
      <div className='drag-wrapper'>
        <DragDropContext onDragEnd={result => onDragEnd(result, columns, setColumns)}>
          {Object.entries(columns).map(([columnId, column], index) => {
            return <Column columnId={columnId} column={column} addTask={addTask} />;
          })}
        </DragDropContext>
      </div>
    );
  }
};

export default App;
