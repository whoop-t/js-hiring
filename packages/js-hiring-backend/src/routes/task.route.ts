const express = require('express');
const taskController = require('../controller/task.controller');

const router = express.Router();

router.route('/').get(taskController.getTasks).post(taskController.createTask).patch(taskController.updateTask);

module.exports = router;
