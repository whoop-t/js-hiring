const TaskRoute = require('./routes/task.route');
import bodyParser from 'body-parser';
import express from 'express';
import cors from 'cors';

export function createApp() {
  const app = express();
  app.use(cors({ origin: 'http://localhost:3000' }));
  app.use(bodyParser.json());
  app.use('/', TaskRoute);
  return app;
}
