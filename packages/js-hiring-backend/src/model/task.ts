import mongoose from 'mongoose';

export interface ITask extends mongoose.Document {
  title: string;
  description: string;
  status: string;
  board_id: string;
}

export const TaskSchema = new mongoose.Schema({
  title: String,
  description: String,
  status: String,
  board_id: String
});

const Task = mongoose.model<ITask>('Task', TaskSchema);
export default Task;
