import { Request, Response, NextFunction } from 'express';
import Task, { ITask } from '../model/task';

/**
 * getTasks
 * @param req
 * @param res
 * @desc Query DB and return all the tasks
 */
const getTasks = async (req: Request, res: Response) => {
  const tasks = await Task.find({});
  res.send({ tasks });
};

/**
 * createTask
 * @param req
 * @param res
 * @desc Create task model and POST to the DB
 */
const createTask = async (req: Request, res: Response) => {
  const { body } = req;
  const task = await Task.create(body);
  task.save();
  res.send({ task });
};

/**
 * updateTask
 * @param req
 * @param res
 * @desc Update induvidual task when switching to new board
 */
const updateTask = async (req: Request, res: Response) => {
  const { boardId, status, id } = req.body;
  const update = { board_id: boardId, status: status };
  const filter = { _id: id };
  const task = await Task.findOneAndUpdate(filter, update);
  res.send({ task });
};

module.exports = {
  getTasks,
  createTask,
  updateTask
};
